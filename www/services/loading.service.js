(function(window, angular, ionic) {
  'use strict';

  angular
      .module('app')
      .factory('loadingService', loadingService);

  loadingService.$inject = ['$ionicLoading'];
  function loadingService($ionicLoading) {

    var service = {
      show: show,
      hide: hide
    };

    return service;

    function show() {
      $ionicLoading.show({
        template: '<p><img src="img/three-dots.svg" width="50" /></p><p>Cargando</p>',
      });
    }

    function hide() {
      $ionicLoading.hide();
    }
  }

})(window, window.angular, window.ionic);
