(function(window, angular) {
  'use strict';

  angular
    .module('app.layouts', [
      'app.layouts.sidemenu'
    ]);

})(window, window.angular);
