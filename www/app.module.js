(function (window, angular) {
  'use strict';

  angular
    .module('app', [
      'ionic',
      'app.layouts',
      'app.modules'
    ]);
})(window, window.angular);
