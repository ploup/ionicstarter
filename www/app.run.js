(function (window, angular, ionic) {
  'use strict';

  angular
    .module('app')
    .run(appRun);

  appRun.$inject = ['$ionicPlatform'];
  function appRun($ionicPlatform) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }

      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  }
})(window, window.angular, window.ionic);
