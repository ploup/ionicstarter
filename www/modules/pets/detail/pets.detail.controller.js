(function(window, angular) {
  'use strict';

  angular
    .module('app.modules.pets')
    .controller('PetsDetailController', PetsDetailController);

  PetsDetailController.$inject = ['$stateParams'];
  function PetsDetailController($stateParams) {
    var vm = this;

    /*Propiedades*/
    vm.data = $stateParams.data;
    console.log(vm.data);

    /*Methods*/

    activate();

    function activate() {

    }
  }

})(window, window.angular);
