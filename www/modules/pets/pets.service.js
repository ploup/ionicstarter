(function (window, angular) {
  'use strict';

  angular
    .module('app.modules.pets')
    .factory('petsService', petsService);

  petsService.$inject = ['$http', '$q'];
  function petsService($http, $q) {

    var service = {
      getPets: getPets
    };

    return service;

    function getPets(status) {
      var defered = $q.defer();
      var promise = defered.promise;
      $http({
          method: 'GET',
          url: 'http://petstore.swagger.io/v2/pet/findByStatus?status='+status,
          headers: {
            'Content-Type': 'application/json'
          }
        })
      .success(function(data) {
        defered.resolve(data);
      })
      .error(function(err) {
        defered.reject(err)
      });
      return promise;
    }
  }
})(window, window.angular);
