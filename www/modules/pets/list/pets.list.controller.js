(function(window, angular) {
  'use strict';

  angular
    .module('app.modules.pets')
    .controller('PetsListController', PetsListController);

  PetsListController.$inject = ['petsService', '$ionicActionSheet', '$filter',
  'loadingService', '$state'];
  function PetsListController(petsService, $ionicActionSheet, $filter,
    loadingService, $state) {
    var vm = this;

    /*Propiedades*/
    vm.selectedOption = 1;
    vm.searchName = "";
    vm.options = [
      { text: "Disponibles", value: 'available'},
      { text: "Pendientes", value:'pending'},
      { text: "Vendidas", value: 'sold'}
    ];
    vm.selectedOptionName = vm.options[vm.selectedOption].text;

    /*Methods*/
    vm.go = go;
    vm.loadMore = loadMore;
    vm.filter = filter;
    vm.search = search;

    activate();

    function activate() {
      _getPets(vm.options[vm.selectedOption].value);
    }

    function _getPets(status) {
      vm.perPage = 10;
      petsService.getPets(status)
        .then(function(data) {
          vm.pets = data;
          if(vm.searchName != "") {
            vm.pets = $filter('filter')(vm.pets, {name: vm.searchName});
          }
          loadingService.hide();
        })
        .catch(function(err) {
          console.log(err);
        });
    }

    function go(pet){
      $state.go('app.petsDetail',{data: pet});
    }

    function loadMore() {
      vm.perPage += 20;
    }

    function filter() {
      var hideSheet = $ionicActionSheet.show({
         buttons: vm.options,
         titleText: 'Filtrar',
         cancelText: 'Cancelar',
         buttonClicked: function(index) {
           loadingService.show();
           vm.selectedOption = index;
           vm.selectedOptionName = vm.options[index].text;
           _getPets(vm.options[index].value);
           hideSheet();
         }
       });
     }

     function search(){
       if(vm.searchName == "") {
         loadingService.show();
         _getPets(vm.options[vm.selectedOption].value);
       }else {
         vm.pets = $filter('filter')(vm.pets, {name: vm.searchName});
       }
     }
   }

})(window, window.angular);
