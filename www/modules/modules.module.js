(function(window, angular) {
  'use strict';

  angular
    .module('app.modules', [
      'app.modules.pets'
    ]);

})(window, window.angular);
