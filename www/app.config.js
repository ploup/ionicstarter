(function (window, angular) {
  'use strict';

  angular
    .module('app')
    .config(appConfig);

  appConfig.$inject = ['$stateProvider', '$urlRouterProvider','$ionicConfigProvider',
'$compileProvider'];
  function appConfig($stateProvider, $urlRouterProvider, $ionicConfigProvider,
  $compileProvider) {

    // Improve Angular JS performance
    $compileProvider.debugInfoEnabled(false);

    $ionicConfigProvider.scrolling.jsScrolling(false);

    $stateProvider
      .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'layout/sidemenu/sidemenu.menu.html'
    })
      .state('app.pets', {
        url: '/pets',
        views: {
          'menuContent': {
            templateUrl: 'modules/pets/list/pets.list.html',
            controller: 'PetsListController as vm'
          }
        }
      })
      .state('app.petsDetail', {
        url: '/pets/detail',
        params: {
          data: null
        },
        views: {
          'menuContent': {
            templateUrl: 'modules/pets/detail/pets.detail.html',
            controller: 'PetsDetailController as vm'
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/pets');
  }
})(window, window.angular);
